//
//  LoadingView.h
//  cos429
//
//  Created by Emily Lancaster on 1/14/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef LOADVIEW_H
#define LOADVIEW_H

#include "ScoredView.h"


@interface LoadingView : UIViewController

@property (retain) UILabel *label;
@property (retain) UIProgressView *progress;
@property (retain) UIImageView *imgview;

@property (retain, readwrite) ScoredView *scoredView;

@property int theHeight;

// Timer stuff
@property (retain) NSTimer *updateTimer;
@property (retain) NSMutableArray *imagesToShow;
@property int lastSeqShown;
@property (retain) NSDate *lastShown;

- (void)changeLoadState:(UIImage *)image;
- (void)finishLoading:(UIImage *)img withWords:(WordList *)words;
- (void)readyForStart;

@end

#endif