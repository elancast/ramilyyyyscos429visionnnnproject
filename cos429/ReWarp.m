//
//  ReWarp.m
//  cos429
//
//  Created by Rafael Romero on 1/13/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Accelerate/Accelerate.h>

#import "ReWarp.h"
#import "ReCluster.h"
#import "ReCommonUtil.h"

@implementation ReWarp

extern void print_matrix( char* desc, __CLPK_integer m, __CLPK_integer n, 
                         float* a, __CLPK_integer lda );
void tileToCoords(Tile_T tile, Coords_T coords);
double *calculateH(int num, Cluster_T *clusters, Tile_T *tiles);
void transformCoords(double* h, int x, int y, double *u, double *v);




void tileToCoords(Tile_T tile, Coords_T coords)
{
    int x = tile->x;
    int y = tile->y;
    
    coords->x = x*100;
    coords->y = y*100;
}

double *calculateH(int num, Cluster_T *clusters, Tile_T *tiles)
{
    // set up the SOURCE points
    Coords_T *from = malloc(num * sizeof(Coords_T));
    for (int i = 0; i < num; i++)
        from[i] = [ReCluster computeClusterCenter:clusters[i]];
    
    // set up the DESTINATION points
    Coords_T *to = malloc(num * sizeof(Coords_T));
    for (int i = 0; i < num; i++)
        to[i] = malloc(sizeof(struct Coords));
    for (int i = 0; i < num; i++)
    {
        if (tiles[i]->clusterId != i) NSLog(@"PANIC!");
        tileToCoords(tiles[i], to[i]);
    }
    
    // build up the M x N, 2n x 9, column-major order matrix A
    __CLPK_integer M = 2*num;
    __CLPK_integer N = 9;
    __CLPK_real *A = malloc(M*N * sizeof(__CLPK_real)); // offset = row + column*NUMROWS
    bzero(A, M*N * sizeof(__CLPK_real));
    for (int i = 0; i < num; i++)
    {
        int x = from[i]->x;
        int y = from[i]->y;
        int u = to[i]->x;
        int v = to[i]->y;
        
        A[2*i   + 0  *M] = (__CLPK_real) -x;
        A[2*i   + 1  *M] = -y;
        A[2*i   + 2  *M] = -1;
        
        A[2*i+1 + 3  *M] = -x;
        A[2*i+1 + 4  *M] = -y;
        A[2*i+1 + 5  *M] = -1;
        
        A[2*i   + 6  *M] = u*x;
        A[2*i   + 7  *M] = u*y;
        A[2*i   + 8  *M] = u;
        
        A[2*i+1 + 6  *M] = v*x;
        A[2*i+1 + 7  *M] = v*y;
        A[2*i+1 + 8  *M] = v;
        
        //NSLog(@"Mapping from (%d, %d) to (%d, %d)", x,y, u,v);
    }
    
    ///print_matrix("Matrix A", M, N, A, M);
    
    char n = 'N', a = 'A';
    __CLPK_integer lwork = 1000, info;
    __CLPK_real *S = malloc(N * sizeof(__CLPK_real));
    __CLPK_real *VT = malloc(N*N * sizeof(__CLPK_real));
    
    sgesvd_(&n,    // 'N' // char *jobu = 'N':  no columns of U (no left singular vectors) are computed.
            &a,    // 'A' // char *jobvt = 'A':  all N rows of V**T are returned in the array
            
            &M,    // M=2n     // __CLPK_integer *m = The number of rows of the input matrix A.  M >= 0.
            &N,    // N=9      // __CLPK_integer *n = The number of columns of the input matrix A.  N >= 0.
            A,     // MxN=2n*9 // __CLPK_real *a = REAL array, dimension (LDA,N). On entry, the M-by-N matrix A
            &M,  // M=2n     // __CLPK_integer *lda = The leading dimension of the array A.  LDA >= max(1,M)
            S,     // N=9  // __CLPK_real *s = (output) REAL array, dimension (min(M,N))
            // The singular values of A, sorted so that S(i) >= S(i+1)
            NULL,     // NULL // __CLPK_real *u = 
            &M,  // M=2n // __CLPK_integer *ldu = The leading dimension of the array U.
            VT,    // NxN=9x9 // __CLPK_real *vt = If JOBVT = 'A', VT contains the N-by-N orthogonal matrix V**T;
            &N, // N=9 // __CLPK_integer *ldvt = The leading dimension	of the array VT
            malloc(lwork * sizeof(__CLPK_real)),  // arr[1000] // __CLPK_real *work = 
            &lwork,  // 1000 // __CLPK_integer *lwork =  The dimension	of the array WORK
            &info   // __CLPK_integer *info = result
            );
    
    /* Print singular values */
    ///print_matrix( "Singular values", 1, N, S, 1 );
    /* Print right singular vectors */
    ///print_matrix( "Right singular vectors (stored rowwise)", N, N, VT, N );
    
    double *h = malloc(9 * sizeof(double));
    for (int i = 0; i < 9; i++)
        h[i] = VT[8 + i*9];

    return h;
}

void transformCoords(double* h, 
                     int x, int y, 
                     double *u, double *v)
{
    double d1 = x*h[0] + y*h[1] + 1*h[2];
    double d2 = x*h[3] + y*h[4] + 1*h[5];
    double d3 = x*h[6] + y*h[7] + 1*h[8];
    
    *u = d1/d3;
    *v = d2/d3;
}

typedef struct BigPixel
{
    double R, G, B;
} BigPixel;

+ (Pixel *)deWarp_image:(CGImageRef)imgRef 
             usingTiles:(Tile_T *)tiles
             onClusters:(ClusterBag_T)clusterBag 
               newWidth:(int *)newW newHeight:(int *)newH
{    
    NSMutableArray *nsClusters = clusterBag->clusters;
    int num = [nsClusters count];
    
    // set up clusters for easy alloc
    Cluster_T clusters[num];
    for (int i = 0; i < num; i++)
    {
        NSData *nsCluster = [nsClusters objectAtIndex:i];
        clusters[i] = [ReCommonUtil structFromData:nsCluster];
    }
    
    double *H = calculateH(num, clusters, tiles);
    for (int i = 0; i < 9; i++)
        NSLog(@"%f", H[i]);

    double u, v;
    
    int w = CGImageGetWidth(imgRef);
    int h = CGImageGetHeight(imgRef);
    Pixel *pixels = [ReCameraStuff getPixels:imgRef];
    
    int x[] = {0, w, w, 0};
    int y[] = {0, 0, h, h};
    
    double minU = 1000, minV = 1000, maxU = -1, maxV = -1;
    for (int i = 0; i < 4; i++)
    {
        transformCoords(H, x[i], y[i], &u, &v);
        minU = MIN(minU, u);
        minV = MIN(minV, v);
        maxU = MAX(maxU, u);
        maxV = MAX(maxV, v);
    }
    minU -= 5; minV -= 5; maxU += 5; maxV += 5;
    NSLog(@"%f %f %f %f", minU, minV, maxU, maxV);
    
    int dimU = (int)(maxU - minU);
    int dimV = (int)(maxV - minV);
    BigPixel **temp = malloc(dimU * sizeof(BigPixel *));
    for (int i = 0; i < dimU; i++)
        temp[i] = malloc(dimV * sizeof(BigPixel));
    for (int iu = 0; iu < dimU; iu++)
        for (int iv = 0; iv < dimV; iv++)
            temp[iu][iv].R = temp[iu][iv].G = temp[iu][iv].B = 0;
    
    ///double ksum[dimU][dimV];
    double **ksum = malloc(dimU * sizeof(double*));
    for (int i = 0; i < dimU; i++)
        ksum[i] = malloc(dimV * sizeof(double));
    for (int iu = 0; iu < dimU; iu++)
        for (int iv = 0; iv < dimV; iv++)
            ksum[iu][iv] = 0;
    ///bzero(ksum, dimU*dimV*sizeof(double));
    
    double SIGMA = 0.5, WID = 3*SIGMA;
    for (int iw = 0; iw < w; iw++) {
        if (iw % 200 == 0)
            NSLog(@"%d out of %d", iw, w);
        
        for (int ih = 0; ih < h; ih++) {
            transformCoords(H, iw, ih, &u, &v);
            u -= minU;
            v -= minV;
            
            // splat
            // neighborhood
            int ulo = ceil(u - WID), uhi = floor(u + WID);
            int vlo = ceil(v - WID), vhi = floor(v + WID);
            
            for (int iu = ulo; iu <= uhi; iu++) {
                for (int iv = vlo; iv <= vhi; iv++) 
                {
                    // check bounds
                    if (iu < 0 || iv < 0 || iu >= dimU || iv >= dimV)
                        continue;
                    
                    // distance
                    double du = iu - u;
                    double dv = iv - v;
                    double d2 = du*du + dv*dv;
                    
                    // weight
                    double k = exp(-1*d2 / (2*SIGMA*SIGMA) );
                    
                    // get pixel
                    BigPixel *dest = &temp[iu][iv];
                    Pixel *src = [ReCameraStuff getPixel:pixels width:w row:ih col:iw];
                    dest->R += k*src->red;
                    dest->G += k*src->green;
                    dest->B += k*src->blue;
                    
                    ksum[iu][iv] += k; // keep tab of weights
                }
            }
        }
    }
    
    Pixel *warped = malloc( dimU * dimV * sizeof(Pixel) );
    for (int iu = 0; iu < dimU; iu++) {
        for (int iv = 0; iv < dimV; iv++) {
            Pixel *pixel = [ReCameraStuff getPixel:warped width:dimU row:iv col:iu];
            pixel->red = pixel->green = pixel->blue = 0;
            pixel->alpha = 1.0;
        }
    }
    
    for (int iu = 0; iu < dimU; iu++)
        for (int iv = 0; iv < dimV; iv++)
            if (ksum[iu][iv] != 0)
            {
                Pixel *dest = [ReCameraStuff getPixel:warped width:dimU row:iv col:iu];
                dest->red = temp[iu][iv].R / ksum[iu][iv];
                dest->green = temp[iu][iv].G / ksum[iu][iv];
                dest->blue = temp[iu][iv].B / ksum[iu][iv];
            }
           
    for (int i = 0; i < num; i++) {
        for (int j = 0; j < [clusters[i]->coords count]; j++)
        {
            NSData *data = [clusters[i]->coords objectAtIndex:j];
            Coords_T coords = (Coords_T) [ReCommonUtil structFromData:data];
            transformCoords(H, coords->x, coords->y, &u, &v);
            u -= minU;
            v -= minV;
            coords->x = (int)u;
            coords->y = (int)v;
        }
    }
    
    /*
    for (int i = 0; i < num; i++) {
        Coords_T center = [ReCluster computeClusterCenter:clusters[i]];
        NSLog(@"Center at (%d, %d)", center->x, center->y);
    }
    */
    
    *newW = dimU;
    *newH = dimV;
    return warped;
    
}




#define min(a,b) ((a)>(b)?(b):(a))
/* Parameters */
#define M 6 // rows
#define N 5 // columns
#define LDA M
#define LDU M
#define LDVT N
/* Main program */
int ReWarp_main(void) {
    /* Locals */
    __CLPK_integer m = M, n = N, lda = LDA, ldu = LDU, ldvt = LDVT, info;
    /* Local arrays */
    float s[N], u[LDU*M], vt[LDVT*N];
    float a[LDA*N] = {
        8.79f, 6.11f, -9.15f, 9.57f, -3.49f, 9.84f,
        9.93f, 6.91f, -7.93f, 1.64f, 4.02f, 0.15f,
        9.83f, 5.04f, 4.86f, 8.83f, 9.80f, -8.99f, 
        5.45f, -0.27f, 4.85f, 0.74f, 10.00f, -6.02f,
        3.16f, 7.98f, 3.01f, 5.80f, 4.27f, -5.31f
        
//        8.79f,  9.93f,  9.83f, 5.45f,  3.16f,
  //      6.11f,  6.91f,  5.04f, -0.27f,  7.98f,
    //    -9.15f, -7.93f,  4.86f, 4.85f,  3.01f,
      //  9.57f,  1.64f,  8.83f, 0.74f,  5.80f,
        //-3.49f,  4.02f,  9.80f, 10.00f,  4.27f,
        //9.84f,  0.15f, -8.99f, -6.02f, -5.31f
    };
    /* Executable statements */
    printf( "LAPACKE_sgesvd (row-major, high-level) Example Program Results\n" );
    /* Compute SVD */
    char A = 'A'; __CLPK_integer lwork = 1000;
    info = sgesvd_(&A,    // 'N' // char *jobu = 'N':  no columns of U (no left singular vectors) are computed.
                   &A,    // 'A' // char *jobvt = 'A':  all N rows of V**T are returned in the array
                   
                   &m,    // M=2n     // __CLPK_integer *m = The number of rows of the input matrix A.  M >= 0.
                   &n,    // N=9      // __CLPK_integer *n = The number of columns of the input matrix A.  N >= 0.
                   a,     // MxN=2n*9 // __CLPK_real *a = REAL array, dimension (LDA,N). On entry, the M-by-N matrix A
                   &lda,  // M=2n     // __CLPK_integer *lda = The leading dimension of the array A.  LDA >= max(1,M)
                   s,     // N*N=9*9  // __CLPK_real *s = (output) REAL array, dimension (min(M,N))
                                // The singular values of A, sorted so that S(i) >= S(i+1)
                   u,     // NULL // __CLPK_real *u = 
                   &ldu,  // M=2n // __CLPK_integer *ldu = The leading dimension of the array U.
                   vt,    // NxN=9x9 // __CLPK_real *vt = If JOBVT = 'A', VT contains the N-by-N orthogonal matrix V**T;
                   &ldvt, // N=9 // __CLPK_integer *ldvt = The leading dimension	of the array VT
                   malloc(lwork * sizeof(__CLPK_real)),  // arr[1000] // __CLPK_real *work = 
                   &lwork,  // 1000 // __CLPK_integer *lwork =  The dimension	of the array WORK
                   &info   // __CLPK_integer *info = result
                   );
    
    /*
     int sgesvd_(char *jobu, char *jobvt, __CLPK_integer *m, __CLPK_integer *n, 
     __CLPK_real *a, __CLPK_integer *lda, __CLPK_real *s, __CLPK_real *u, __CLPK_integer *ldu, __CLPK_real *vt, 
     __CLPK_integer *ldvt, __CLPK_real *work, __CLPK_integer *lwork, __CLPK_integer *info) __OSX_AVAILABLE_STARTING(__MAC_10_2,__IPHONE_4_0);     
     */
    
    /* Check for convergence */
    if( info > 0 ) {
        printf( "The algorithm computing SVD failed to converge.\n" );
        exit( 1 );
    }
    /* Print singular values */
    print_matrix( "Singular values", 1, n, s, 1 );
    /* Print left singular vectors */
    print_matrix( "Left singular vectors (stored columnwise)", m, n, u, ldu );
    /* Print right singular vectors */
    print_matrix( "Right singular vectors (stored rowwise)", n, n, vt, ldvt );
    exit( 0 );
} /* End of LAPACKE_sgesvd Example */
/* Auxiliary routine: printing a matrix */
void print_matrix( char* desc, __CLPK_integer m, __CLPK_integer n, float* a, __CLPK_integer lda ) {
    __CLPK_integer i, j;
    printf( "\n %s\n", desc );
    for( i = 0; i < m; i++ ) {
        for( j = 0; j < n; j++ ) printf( " %12.6f", a[i+j*lda] );
        printf( "\n" );
    }
}

@end
