//
//  CameraStuff.m
//  cos429
//
//  Created by Emily Lancaster on 1/2/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "ReCommonUtil.h"
#import "ReCameraStuff.h"
#import "ReLetterz.h"
#import "ReCluster.h"
#import "ReMst.h"
#import "ReWarp.h"
#import "ReWords.h"

@implementation ReCameraStuff
@synthesize imgProp;

@synthesize loading = _loading;

@synthesize loadingImage = _loadingImage;

enum { TILE_SIZE = 309 };

/**
 * Initialization stuff
 */
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        // Too bad we don't have any....
        
        imgProp = nil;
    }
    
    return self;
}

/**
 * Returns an array of pixels for the specified CGImageRef
 */
+ (Pixel *)getPixels:(CGImageRef)CGImage
{
    CGDataProviderRef provider = CGImageGetDataProvider(CGImage);
    NSData *data = (NSData *)CGDataProviderCopyData(provider);
    Pixel *pixels = (Pixel *)[data bytes];
    [data autorelease];
    return pixels;
}

/**
 * Returns the pixel for the array of pixels with width w at row r and column c
 */
+ (Pixel *)getPixel:(Pixel *)pixels width:(size_t)w row:(size_t)r col:(size_t)c
{
    return &pixels[r * w + c];
}

+ (void)drawTiles:(Tile_T)tile inPixels:(Pixel *)pixels withW:(int)w off:(bool)b
{
    if (tile == nil || tile->visited == b) return;
    tile->visited = b;
    Coords coords;
    coords.x = tile->x * TILE_SIZE;
    coords.y = tile->y * TILE_SIZE;
    [ReLetterz drawTile:tile onPixels:pixels start:&coords size:TILE_SIZE
        width:w];
    [ReCameraStuff drawTiles:tile->up inPixels:pixels withW:w off:b];
    [ReCameraStuff drawTiles:tile->down inPixels:pixels withW:w off:b];
    [ReCameraStuff drawTiles:tile->left inPixels:pixels withW:w off:b];
    [ReCameraStuff drawTiles:tile->right inPixels:pixels withW:w off:b];
}

+ (void)adjustTileNums:(Tile_T)tile subMin:(Coords_T)min off:(bool)bad
    Max:(Coords_T)max
{
    if (tile == nil || tile->visited == bad) return;
    tile->visited = bad;
    tile->x -= min->x;
    tile->y -= min->y;
    if (tile->x > max->x) max->x = tile->x;
    if (tile->y > max->y) max->y = tile->y;
    [ReCameraStuff adjustTileNums:tile->up subMin:min off:bad Max:max];
    [ReCameraStuff adjustTileNums:tile->down subMin:min off:bad Max:max];
    [ReCameraStuff adjustTileNums:tile->left subMin:min off:bad Max:max];
    [ReCameraStuff adjustTileNums:tile->right subMin:min off:bad Max:max];
}

+ (void)setTileNums:(Tile_T)tile X:(int)x Y:(int)y withMin:(Coords_T)min
    of:(bool)b
{
    if (tile == nil || tile->visited == b) return;
    tile->visited = b;
    tile->x = x;
    tile->y = y;
    if (tile->x < min->x) min->x = tile->x;
    if (tile->y < min->y) min->y = tile->y;

    [ReCameraStuff setTileNums:tile->right X:x+1 Y:y   withMin:min of:b];
    [ReCameraStuff setTileNums:tile->left  X:x-1 Y:y   withMin:min of:b];
    [ReCameraStuff setTileNums:tile->up    X:x   Y:y-1 withMin:min of:b];
    [ReCameraStuff setTileNums:tile->down  X:x   Y:y+1 withMin:min of:b];
}

+ (Pixel *)drawBoardAsSeen:(Tile_T)tile withW:(int *)w withH:(int *)h
{
    // Set the coordinates of the tiles...
    Coords min;
    min.x = 0;
    min.y = 0;
    Coords bounds;
    bounds.x = 0;
    bounds.y = 0;
    [ReCameraStuff setTileNums:tile X:0 Y:0 withMin:&min of:!tile->visited];
    [ReCameraStuff adjustTileNums:tile subMin:&min off:!tile->visited
        Max:&bounds];

    // Find bounds of tiles
    //[ReCameraStuff findTileMax:tile inCoords:&bounds];

    // Make pixels and draw
    int ww = TILE_SIZE * (bounds.x + 1);
    int hh = TILE_SIZE * (bounds.y + 1);
    Pixel *pixels = malloc(sizeof(Pixel) * ww * hh);
    bzero(pixels, sizeof(Pixel) * ww * hh);
    [ReCameraStuff drawTiles:tile inPixels:pixels withW:ww off:!tile->visited];
    *w = ww; *h = hh;
    return pixels;
}

/**
 * Returns a new UIImage * for the pixels data
 */
+ (UIImage *)getImage:(Pixel *)pixels width:(size_t)w height:(size_t)h
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef ctxtRef = CGBitmapContextCreate(pixels, w, h, 8, 
        sizeof(Pixel) * w, colorSpace, kCGImageAlphaNoneSkipLast);
    CGImageRef imgRef = CGBitmapContextCreateImage(ctxtRef);
    UIImage *img = [UIImage imageWithCGImage:imgRef];
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(ctxtRef);
    CGImageRelease(imgRef);
    return img;
}

- (void)assertEqualityBetween:(UIImage *)i1  and:(UIImage *)i2
{
    // Make sure same width and height...
    assert(CGImageGetWidth(i1.CGImage) == CGImageGetWidth(i2.CGImage));
    assert(CGImageGetHeight(i1.CGImage) == CGImageGetHeight(i2.CGImage));
    size_t h = CGImageGetHeight(i1.CGImage);
    size_t w = CGImageGetWidth(i1.CGImage);
    
    // Test that equivalent
    Pixel* pixels1 = [ReCameraStuff getPixels:i1.CGImage];
    Pixel* pixels2 = [ReCameraStuff getPixels:i2.CGImage];
    for (int r = 0; r < h; r++) {
        for (int c = 0; c < w; c++) {
            Pixel *pixel = [ReCameraStuff getPixel:pixels1 width:w row:r col:c];
            Pixel *pixel2 = [ReCameraStuff getPixel:pixels2 width:w row:r col:c];
            assert(pixel->red == pixel2->red);
            assert(pixel->green == pixel2->green);
            assert(pixel->blue == pixel2->blue);
        }
    }
}

- (WordList *)cameraMain:(UIImage *)img
{
    // Sorry
    if (img == nil || img.CGImage == nil) {
        NSLog(@"Invalid Image - cannot be null");
        return nil;
    }
    
    // Convert to array of pixels yo
    Pixel *pixels = [ReCameraStuff getPixels:img.CGImage];
    if (pixels == nil) {
        NSLog(@"Cannot convert image to array of pixels! Sad.");
        return nil;
    }
    
    ///ReWarp_main();
    ///return pixels;
    
    // Cluster these pixels - expects this to be an array of Coords yay
    int w = CGImageGetWidth(img.CGImage), h = CGImageGetHeight(img.CGImage);
    ClusterBag_T clusters = [ReCluster getClustersForImg:pixels 
                                                  height:h width:w];
    if (clusters == nil) {
        NSLog(@"Error finding clusters ");
        return nil;
    }
    
    Pixel *lummedPixels = [ReCluster getLummedPixels:pixels 
                                              height:h width:w 
                                          clusterBag:clusters];
    UIImage *lummedImg = [ReCameraStuff getImage:lummedPixels width:w height:h];
    
    // And show.
    [_loading changeLoadState:lummedImg];
        
    // Get centers
    NSMutableArray *centers = [ReMst computeCentersFromClusterBag:clusters];
    if (centers == nil) {
        NSLog(@"Something went wrong here!");
        return nil;
    }
    
    Pixel *results = [ReMst drawCenters:centers OntoImage:lummedImg.CGImage];
    
    // Find the MST oooooh
    Tile_T *arr = [ReMst findMstForCenters:centers resultsOn:results W:w];
    if (arr == nil) {
        NSLog(@"YOU ARRRR IN TROUBLE");
        return nil;
    }
    Tile_T msft = arr[0];
    if (msft == nil) {
        NSLog(@"MSFT HATES USSSSS");
        return nil;
    }
    
    // And show.
    UIImage *mstImg = [ReCameraStuff getImage:results width:w height:h];
    [_loading changeLoadState:mstImg];

    UIImage *mostRecent = [ReCameraStuff getImage:results width:w height:h];
    
    int newW, newH;
    Pixel *newPixels = nil;
    if (clusters->clusters.count >= 4) {
        newPixels = [ReWarp deWarp_image:mostRecent.CGImage 
            usingTiles:arr onClusters:clusters newWidth:&newW newHeight:&newH];
    }
    imgProp = newPixels == nil ? mstImg
        : [ReCameraStuff getImage:newPixels width:newW height:newH ];
    [_loading changeLoadState:imgProp];

    // Find the letters...
    [ReLetterz fillLettersForTile:msft withClusters:clusters];
    int cw, ch;
    Pixel *correct = [ReCameraStuff drawBoardAsSeen:msft withW:&cw withH:&ch];
    imgProp = [ReCameraStuff getImage:correct width:cw height:ch];
    
    // And score!
    WordList *list = [ReWords scoreBoard:msft];

    // Be happy!
    return list;
}

- (void)startComputation:(UIImage *)img
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    // Show the selected image...
    [_loading changeLoadState:img];

    // Do vision
    WordList *list = [self cameraMain:img];

    // We're done.
    [_loading finishLoading:imgProp withWords:list];

    [pool release];
}

- (void)imagePickerController:(UIImagePickerController *)picker 
    didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];

    // Close the view
    _loadingImage = true;
    [picker dismissModalViewControllerAnimated:true];

    // Start a thread to do computation
    imgProp = img;
    [NSThread detachNewThreadSelector:@selector(startComputation:) 
        toTarget:self withObject:img];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    _loadingImage = false;
    [picker dismissModalViewControllerAnimated:true];
}




@end
