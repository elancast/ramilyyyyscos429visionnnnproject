//
//  ReWarp.h
//  cos429
//
//  Created by Rafael Romero on 1/13/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Accelerate/Accelerate.h>
#import <Foundation/Foundation.h>
#import "ReCluster.h"
#import "ReMst.h"

@interface ReWarp : NSObject

+ (Pixel *)deWarp_image:(CGImageRef)imgRef 
             usingTiles:(Tile_T *)tiles
             onClusters:(ClusterBag_T)clusterBag
               newWidth:(int *)newW newHeight:(int *)newH;

int ReWarp_main(void);

@end
