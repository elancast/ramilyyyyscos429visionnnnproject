//
//  ReWords.m
//  cos429
//
//  Created by Emily Lancaster on 1/12/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "ReWords.h"

@implementation ReWords

enum {
    MAX_LINE_LENGTH = 20,
    NUM_WORDS = 178691,
    TILE_LEN = sizeof(struct Tile),
    TILE_LIST_SIZE = 100
};

static char RARE[] = { 'J', 'K', 'Q', 'X', 'Z' };

// The word list -- nil if not loaded
static WordList *list = nil;

// Loads the words list into memory and returns true if everything was happy
+ (bool)loadWordList
{
    // Open the file and have a buf to read lines
    char buf[MAX_LINE_LENGTH];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"TWL_2006_ALPHA" 
                                                 ofType:@"txt"];
    FILE *file = fopen([path cStringUsingEncoding:1], "r");
    if (file == nil) {
        NSLog(@"Cannot find file: %s", [path cStringUsingEncoding:1]);
        return false;
    }
    
    // Init a data structure to store the words list
    char **words = malloc(sizeof(char *) * NUM_WORDS);
    if (words == nil) {
        NSLog(@"Error mallocing struct for words");
        return false;
    }
    int count = 0;
    
    while (count < NUM_WORDS) {
        char *ret = fgets(buf, MAX_LINE_LENGTH, file);
        if (ret == nil) break;
        
        // Malloc enough memory to copy it over...
        int len = strlen(buf) - 1;
        if (len < 2) continue;
        char *cpy = malloc(sizeof(char) * len);
        if (cpy == nil) {
            NSLog(@"Error mallocing enough for string %d", count);
            // TODO: free...
            return false;
        }
        
        // Copy and store -- overwrite \n with null cahracter
        strncpy(cpy, buf, len);
        cpy[len - 1] = '\0';
        words[count++] = cpy;
        // if (count % 10000 == 0) NSLog(@"Read %d words!", count);
    }
    
    // Create a wordlist struct...
    list = malloc(sizeof(WordList));
    if (list == nil) {
        NSLog(@"Error mallocing for struct...which kinda sucks bc got words..");
        // TODO: FREEEEE
        return false;
    }
    list->words = words;
    list->len = count;
    NSLog(@"Done reading %d words", list->len);
    return true;
}

// Returns true if the dictionary contains the specified word
+ (bool)containsWord:(char *)word
{
    if (list == nil && ![ReWords loadWordList]) return false;
    
    int min = 0, max = list->len - 1;
    while (min <= max) {
        // Check middle
        int med = (min + max) / 2;
        int comp = strcmp(word, list->words[med]);
        if (comp == 0) return true;
        
        // Advance
        if (comp < 0) max = med - 1;
        else min = med + 1;
    }
    return false;
}

// Returns the next (right/down) in that direction
+ (Tile_T)getNext:(Tile_T)tile isVert:(bool)vert
{
    return vert ? tile->down : tile->right;
}

// Returns the previous (left/up) in that direction
+ (Tile_T)getPrev:(Tile_T)tile isVert:(bool)vert
{
    return vert ? tile->up : tile->left;
}

// Returns true if that dir has already been checked for the tile
+ (bool)hasBeenDone:(Tile_T)tile isVert:(bool)vert
{
    return vert ? tile->vert : tile->horiz;
}

// Adds the tile to the list of tiles, expanding if necessary
+ (void)addToList:(TileList_T)list tile:(Tile_T)t
{
    if (list->len == list->cap) {
        list->tiles = realloc(list->tiles, sizeof(Tile_T) * list->cap * 2);
        list->cap = list->cap * 2;
    }
    list->tiles[list->len++] = t;
}

// Scans over a tile in the direction specified to return a word. NULL if just
// that letter in the specified direction
+ (char *)doScan:(Tile_T)tile isVert:(bool)vert addTiles:(TileList_T)list
{
    if ([ReWords hasBeenDone:tile isVert:vert]) return nil;
    
    // Backtrack to the first of the set...
    Tile_T start = tile;
    while ([ReWords getPrev:start isVert:vert] != nil) {
        start = [ReWords getPrev:start isVert:vert];
    }
    
    // Initialize a string for the tileys
    char buf[MAX_LINE_LENGTH];
    int len = 0;
    do {
        // Store the letter and mark seen for this direction
        buf[len++] = start->rafi;
        if (vert) start->vert = true;
        else start->horiz = true;
        
        // Add the other direction to arr if it hasn't been seen
        if (!start->vert || !start->horiz) [ReWords addToList:list tile:start];
        start = [ReWords getNext:start isVert:vert];
    } while (start != nil);
    buf[len] = '\0';
    
    // IS IT A WORD???
    if (len < 2) return nil;
    char *cpy = malloc(sizeof(char) * (len + 1));
    strcpy(cpy, buf);
    return cpy;
}

// Returns the score of the word
+ (void)addToWordList:(WordList *)list aWord:(char *)word
{
    if (list->len == list->cap) {
        list->cap = list->cap * 2;
        list->words = realloc(list->words, sizeof(char *) * list->cap);
        list->scores = realloc(list->scores, sizeof(int) * list->cap);
    }
    
    // Find the length of the word and check if it is one
    int len = strlen(word);
    int score = len;
    if (len > 3) score += len;
    if (len > 6) score += len;
    bool isWord = [ReWords containsWord:word];

    NSLog(@"Got word: %s isWord: %d\n", word, isWord);
    list->words[list->len] = word;
    list->scores[list->len] = isWord ? score : -score;
    list->totalScore += list->scores[list->len];
    list->len++;
}

+ (void)sortWordsByScore:(WordList *)words
{
    for (int swap = 0; swap < words->len; swap++) {
        int best = swap, bestVal = words->scores[swap];
        for (int i = swap + 1; i < words->len; i++) {
            if (words->scores[i] > bestVal) {
                best = i;
                bestVal = words->scores[i];
            }
        }
        
        if (best == swap) continue;
        int temp = words->scores[swap];
        char *tempw = words->words[swap];
        words->scores[swap] = words->scores[best];
        words->words[swap] = words->words[best];
        words->scores[best] = temp;
        words->words[best] = tempw;
    }
}

// Scores a board with the tile in it
+ (WordList *)scoreBoard:(Tile_T)tile
{
    // Create a tile list
    TileList tlist;
    tlist.tiles = malloc(sizeof(Tile_T) * TILE_LIST_SIZE);
    tlist.cap = TILE_LIST_SIZE;
    tlist.len = 0;
    
    // Create a word list
    WordList *wordList = malloc(sizeof(struct WordList));
    wordList->cap = 50;
    wordList->totalScore = 0;
    wordList->len = 0;
    wordList->scores = malloc(sizeof(int) * wordList->cap);
    wordList->words = malloc(sizeof(char *) * wordList->cap);
    
    // Go through all tiles and consider and stuff
    [ReWords addToList:&tlist tile:tile];
    for (int i = 0; i < tlist.len; i++) {
        tile = tlist.tiles[i];
        char *ret = [ReWords doScan:tile isVert:!tile->vert addTiles:&tlist];
        if (ret == nil) continue;
        [ReWords addToWordList:wordList aWord:ret];
    }
    [ReWords sortWordsByScore:wordList];
    return wordList;
}

/*
Testing function to call scoreBoard on the following:
  B
H E L L O
  N   I
  T O G G L E
      H
      T I N T E D
And logs the score to NSLog
*/
+ (void)test
{
    // Let's create one word and see if it's recognized...
    char *word = "HELLO";
    int len = strlen(word);
    Tile_T etile = nil, ttile = nil;
    Tile_T prev = nil, mid = nil;
    for (int i = 0; i < len; i++) {
        Tile_T tile = [ReMst getNewTile:0 withChar:word[i]];
        if (prev != nil) prev->right = tile;
        tile->left = prev;
        prev = tile;
        if (i == 3) mid = prev;
        if (prev->rafi == 'E') etile = prev;
    }
    
    // Add a word going down from the l
    char *light = "LIGHT";
    len = strlen(light);
    prev = mid;
    Tile_T gtile = nil, lightttile = nil;
    for (int i = 1; i < len; i++) {
        Tile_T tile = [ReMst getNewTile:0 withChar:light[i]];
        if (prev != nil) prev->down = tile;
        tile->up = prev;
        prev = tile;
        if (prev->rafi == 'G') gtile = prev;
        if (prev->rafi == 'T') lightttile = prev;
    }

    // Add a word across in the middle
    prev = nil;
    word = "TOGGLE";
    len = strlen(word);
    for (int i = 0; i < len; i++) {
        Tile_T tile = [ReMst getNewTile:0 withChar:word[i]];
        if (gtile != nil && word[i] == gtile->rafi) 
            { tile = gtile; gtile = nil; }
        if (prev != nil) prev->right = tile;
        tile->left = prev;
        prev = tile;
        if (prev->rafi == 'T') ttile = prev;
    }

    // Add a cycle word...
    prev = nil;
    word = "BENT";
    len = strlen(word);
    for (int i = 0; i < len; i++) {
        Tile_T tile = [ReMst getNewTile:0 withChar:word[i]];
        if (ttile != nil && word[i] == ttile->rafi) 
            { tile = ttile; ttile = nil; }
        else if (etile != nil && word[i] == etile->rafi) 
            { tile = etile; etile = nil; }
        if (prev != nil) prev->down = tile;
        tile->up = prev;
        prev = tile;
    }
    
    // And jsut another word to make sure this ACTUALLY works...
    prev = lightttile;
    word = "INTED";
    len = strlen(word);
    for (int i = 0; i < len; i++) {
        Tile_T tile = [ReMst getNewTile:0 withChar:word[i]];
        if (prev != nil) prev->right = tile;
        tile->left = prev;
        prev = tile;
    }

    // Test scoreBoard...
    WordList *list = [ReWords scoreBoard:mid];
    NSLog(@"Scored %d", list->totalScore);
}

@end
