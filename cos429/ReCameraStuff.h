//
//  CameraStuff.h
//  cos429
//
//  Created by Emily Lancaster on 1/2/12.
//  Copyright 2012 Princeton University. All rights reserved.
//


#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

#ifndef RECAMERA_H
#define RECAMERA_H

#include "LoadingView.h"

@interface ReCameraStuff : UIImagePickerController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (retain) UIImage *imgProp;

@property (retain, readwrite) LoadingView *loading;

@property bool loadingImage;

// Pixel w00t
typedef struct Pixel {
    Byte red;
    Byte green;
    Byte blue;
    Byte alpha;
}   Pixel;

// Get the array of pixels from a CGImage
+ (Pixel *)getPixels:(CGImageRef)CGImage;

+ (Pixel *)getPixel:(Pixel *)pixels 
              width:(size_t)w 
                row:(size_t)r 
                col:(size_t)c;

@end

#endif
