//
//  LoadingView.m
//  cos429
//
//  Created by Emily Lancaster on 1/14/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

@synthesize imgview = _imgview;

@synthesize label = _label;

@synthesize progress = _progress;

@synthesize theHeight = _theHeight;

@synthesize updateTimer = _updateTimer;
@synthesize imagesToShow = _imagesToShow;
@synthesize lastSeqShown = _lastSeqShown;
@synthesize lastShown = _lastShown;

@synthesize scoredView = _scoredView;

enum {
    TOOLBAR_HEIGHT = 64
};

static const double PAUSE_TIME = 1;

static char *loadingMsgs[] = { "Recognizing tiles...",
    "Finding tile connectivity...", "Adjusting camera...",
    "Identifying letters...", "Done!" };

- (int)getWidth
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

- (int)getHeight
{
    return CGRectGetWidth([[UIScreen mainScreen] bounds]);
}

- (void)readyForStart
{
    _label.text = @"Loading...";
    [_progress setProgress:0];
        
    // Init state vars...
    _lastSeqShown = 0;
    _imagesToShow = [[NSMutableArray alloc] init];
    _updateTimer = nil;
    _lastShown = nil;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Init the label
        int labH = 50;
        _label = [[UILabel alloc] 
            initWithFrame:CGRectMake(0, 0, [self getWidth], labH)];
        _label.backgroundColor = [UIColor blackColor];
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = UITextAlignmentCenter;
        
        // Init the progress bar
        _progress = [[UIProgressView alloc] 
            initWithProgressViewStyle:UIProgressViewStyleBar];
        int h = [self getHeight] > 500 ? 20 : 40;
        [_progress setFrame:CGRectMake(0, 0, [self getWidth] * 0.9, h)];
        [_progress setProgress:0];
        _progress.center = CGPointMake([self getWidth] / 2, h);
        
        // Give the label a place...
        _label.center = CGPointMake([self getWidth] / 2, h * 3 / 2);
        _theHeight = h * 3 / 2;
    }

    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [super loadView];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:_progress];
    [self.view addSubview:_label];
    
    // Fucking shitty iphone development
    //[self.navigationController setToolbarHidden:true];
}


/**
 * Removes the existing image view (if there is one) and then adds this image to
 * a new image view
 */
- (void)showImage:(UIImage *)image
{
    if (image == nil) return;
    if (_imgview != nil) {
        [_imgview removeFromSuperview];
        _imgview = nil;
    }
    
    // Scale to width first
    CGRect screen = [[UIScreen mainScreen] bounds];
    size_t scrH = CGRectGetHeight(screen), scrW = CGRectGetWidth(screen);
    size_t imgW = CGImageGetWidth(image.CGImage);
    size_t imgH = CGImageGetHeight(image.CGImage);
    size_t sclW = scrW;
    size_t sclH = imgH * scrW / imgW;
    
    // Check if height is too large and scale that instead otherwise
    int maxH = scrH - _theHeight * 3;
    if (sclH > maxH) {
        sclH = maxH;
        sclW = imgW * sclH / imgH;
    }

    // Add the image to the view and then view to view
    CGRect scrn = [[UIScreen mainScreen] bounds];
    _imgview = [[UIImageView alloc] 
        initWithFrame:CGRectMake(0.0, 0.0, sclW, sclH) ];
    _imgview.center = 
        CGPointMake(CGRectGetWidth(scrn) / 2, _theHeight * 1.5 + sclH / 2);
    [self.view addSubview:_imgview];
    [_imgview autorelease];
    _imgview.image = image;
}


- (void)showText:(NSNumber *)num
{
    // Change text
    NSString *s = [NSString stringWithFormat:@"%s", loadingMsgs[num.intValue]];
    _label.text = s;
    
    // Show progress
    int value = num.intValue + 1;
    double amt = value * 1.0 / (sizeof(loadingMsgs) / sizeof(char *));
    [_progress setProgress:amt];
}

- (void)updateUI:(NSTimer *)timer
{
    // If shown everything in array, return
//    NSLog(@"In update UI");
    if (_lastSeqShown >= _imagesToShow.count) return;
    
    // If it's not time yet to show the next image, return
    NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
    if (_lastShown != nil) {
        NSTimeInterval time = [now timeIntervalSinceDate:_lastShown];
        NSLog(@"Got interval %f", time);
        if (time < PAUSE_TIME) return;
    }
    
    // Update the UI
    int seq = _lastSeqShown;
    UIImage *image = [_imagesToShow objectAtIndex:_lastSeqShown];
    _lastSeqShown++;
    NSNumber *num = [[NSNumber alloc] initWithInt:seq];
    [self performSelectorOnMainThread:@selector(showImage:) withObject:image
        waitUntilDone:true];
    [self performSelectorOnMainThread:@selector(showText:) withObject:num
        waitUntilDone:true];
    if (_lastShown != nil) [_lastShown release];
    _lastShown = now;
    [num autorelease];
    
    // Check if we're done
    if (_lastSeqShown == (sizeof(loadingMsgs) / sizeof(char *))) {
        [_updateTimer invalidate];
        [_updateTimer release];
        _updateTimer = nil;
        [self.navigationController pushViewController:_scoredView
            animated:true];
    }
}

- (void)changeLoadState:(UIImage *)image
{
    [_imagesToShow addObject:image];
}

- (void)finishLoading:(UIImage *)img withWords:(WordList *)words
{
    [self changeLoadState:img];
    [_scoredView initGameEnd:words withImg:img];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad
{
    [super viewDidLoad];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];
}


/*
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
