//
//  RootViewController.m
//  cos429
//
//  Created by Emily Lancaster on 1/2/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "RootViewController.h"
#import "ReCameraStuff.h"
#import "ReWords.h"
#import "ReLetterz.h"

@interface RootViewController ()
- (void)showImage:(UIImage *)image;
@end

@implementation RootViewController

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

// final view page
@synthesize scoredView = _scoredView;

// Camera
@synthesize cameraStuff;

// View of image
@synthesize imageView;

// Next page
@synthesize loadingView = _loadingView;

@synthesize waitingForClose = _waitingForClose;

@synthesize picker = _picker;


- (void)initBars
{
    // Get an array of fun toolbar items
    UIBarButtonItem *camera = [[UIBarButtonItem alloc] 
        initWithBarButtonSystemItem:UIBarButtonSystemItemCamera 
        target:self action:@selector(handleCameraButtonPress) ];
    [camera autorelease];
        
    // Get some space
    UIBarButtonItem *space = [[UIBarButtonItem alloc]
        initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
            target:nil action:nil];
    [space autorelease];
    
    // Autorelease and add stuff and make the toolbar visible
    [self setToolbarItems:[NSArray arrayWithObjects:space, camera, space, nil]];
    [self.navigationController setToolbarHidden:false];
    [self.navigationController setNavigationBarHidden:true];
}

- (void)initPicker
{
    // Image picker type (are we on a real phone?)
    UIImagePickerControllerSourceType type;
    if ([UIImagePickerController 
        isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES)
        type = UIImagePickerControllerSourceTypeCamera;
    else
        type = UIImagePickerControllerSourceTypePhotoLibrary;
        
    _picker = [[UIImagePickerController alloc] init];
    _picker.sourceType = type;
    _picker.allowsEditing = false;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initBars];

    // Init image view
    imageView = nil;
    
    // Set background to be black
    self.view.backgroundColor = [UIColor blackColor];
    
    // Loading view page
    if (self.loadingView == nil) {
        _loadingView = [[LoadingView alloc] init];
    }
    _waitingForClose = false;
    
    // Scored view page
    if (self.scoredView == nil) {
        _scoredView = [[ScoredView alloc] init];
    }
    _loadingView.scoredView = self.scoredView;
    
    if (_picker == nil) [self initPicker];
}

- (void)handleTestButton
{
    [self.navigationController pushViewController:_scoredView
        animated:true];
}   

/**
 * Removes the existing image view (if there is one) and then adds this image to
 * a new image view
 */
- (void)showImage:(UIImage *)image
{
    if (image == nil) return;
    if (imageView != nil) {
        [self.imageView removeFromSuperview];
        imageView = nil;
    }
    
    // FIgure out if should scale vertically or horizontally
    CGRect rect = [[UIScreen mainScreen] bounds];
    int scrW = CGRectGetWidth(rect), scrH = CGRectGetHeight(rect);
    size_t imgW = CGImageGetWidth(image.CGImage);
    size_t imgH = CGImageGetHeight(image.CGImage);
    size_t w = scrW, h = w * imgH / imgW;
    if (h > scrH) h = scrH, w = h * image.size.width / image.size.height;
    
    // Add the image to the view and then view to view
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, w, h) ];
    imageView.center = self.view.center;
    [self.view addSubview:imageView];
    [imageView autorelease];
    imageView.image = image;
}

- (void)showCameraImage
{
    if (cameraStuff == nil || cameraStuff.imgProp == nil) {
        UIImage *image = [UIImage imageNamed:@"photo.JPG"];
        [self showImage:image];
        return;
    }
    [self showImage:cameraStuff.imgProp];
}

- (void)handleCameraButtonPress
{
//    [self initPicker];
    if (cameraStuff != nil) [cameraStuff release];
    cameraStuff = [[ReCameraStuff alloc] init];
    cameraStuff.loading = _loadingView;
    _picker.delegate = self.cameraStuff;
    
    [_loadingView readyForStart];
    
    // Make picker controller
    _waitingForClose = true;
    [self presentModalViewController:_picker animated:true];
}

- (void)viewWillAppear:(BOOL)animated
{
    if (_waitingForClose) {
        _waitingForClose = false;
        if (cameraStuff.loadingImage) {
            [self.navigationController pushViewController:_loadingView 
                animated:true];
        }
    }
    [self showCameraImage];
    
    [self initBars];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

/*
 // Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 */

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc
{
    [__fetchedResultsController release];
    [__managedObjectContext release];
    [cameraStuff release];
    [super dealloc];
}

@end
