//
//  ReWords.h
//  cos429
//
//  Created by Emily Lancaster on 1/12/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef REWORDS_H
#define REWORDS_H

#include "ReMst.h"
#include "ReCommonUtil.h"
#include "ScoredView.h"

@interface ReWords : NSObject

+ (bool)loadWordList;
+ (bool)containsWord:(char *)word;
+ (WordList *)scoreBoard:(Tile_T)tile;

+ (void)test;

typedef struct TileList *TileList_T;
typedef struct TileList {
    Tile_T *tiles;
    int len, cap;
} TileList;

@end

#endif
