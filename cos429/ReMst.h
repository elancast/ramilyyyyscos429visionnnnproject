//
//  ReMst.h
//  cos429
//
//  Created by Emily Lancaster on 1/10/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef REMST_H
#define REMST_H

#include "ReCluster.h"

@interface ReMst : NSObject

// Tile object to represent the connectivity
typedef struct Tile *Tile_T;
typedef struct Tile {
    int8_t clusterId;
    Tile_T up, down, left, right;
    int x, y;
    BOOL visited;
    
    char rafi;
    bool horiz, vert;
}   Tile;

+ (NSMutableArray *)computeCentersFromClusterBag:(ClusterBag_T)cb;
+ (Pixel *)drawCenters:(NSMutableArray*)centers 
             OntoImage:(CGImageRef)imgRef;

+ (Tile_T *)findMstForCenters:(NSMutableArray *)centers 
                    resultsOn:(Pixel *)pixels W:(int)w;

+ (Tile_T)getNewTile:(int)cluster withChar:(char)rafi;

@end

#endif
