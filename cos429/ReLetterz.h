//
//  ReLetterz.h
//  cos429
//
//  Created by Emily Lancaster on 1/12/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef RELETTER_H
#define RELETTER_H

#include "ReMst.h"
#include "ReCluster.h"
#include "ReCommonUtil.h"
#include "ReCameraStuff.h"

@interface ReLetterz : NSObject

+ (int)getLetterForCluster:(Cluster_T)cluster;

+ (void)fillLettersForTile:(Tile_T)tile withClusters:(ClusterBag_T)clusters;

+ (bool)loadLetters;

+ (void) testClusters:(ClusterBag_T)clusters;

+ (void)drawTile:(Tile_T)tile onPixels:(Pixel *)pixels start:(Coords_T)coords
    size:(int)tsize width:(int)picW;
    

typedef struct Letters *Letters_T;
typedef struct Letter *Letter_T;

typedef struct Letter {
    Coords min;
    Coords max;
    Coords dims;
    bool touched;
    int numCoords;
} Letter;

typedef struct BetterLetter {
    bool *pixels;
    int w, h;
} BetterLetter;

typedef struct Letters {
    bool *pixels;
    int width;
    Letter letters[26];
    BetterLetter ltrs[26 * 4];
} Letters;

@end

#endif
