//
//  ReLetterz.m
//  cos429
//
//  Created by Emily Lancaster on 1/12/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "ReLetterz.h"

@implementation ReLetterz

enum {
    LET_ROWS = 5,
    LET_COLS = 6,
    LUM_THRES = 40,
    NUM_LETTERS = 26,
    I_INDEX = 8,
    I_MOD = 50
};

static const int SYMM_LEN = 7;
static const char SYMMETRICAL[] = { 
    'H', 'I', 'N', 'O', 'S', 'X', 'Z'
};

static Letters_T letters;


+ (void)drawTile:(Tile_T)tile onPixels:(Pixel *)pixels start:(Coords_T)coords
    size:(int)tsize width:(int)picW
{
    int letInd = tile->rafi - 'A';
    BetterLetter *letter = &letters->ltrs[letInd];
    int extraW = tsize - letter->w, extraH = tsize - letter->h;
    int offX = extraW / 2 + coords->x;
    int offY = extraH / 2 + coords->y;
    for (int i = 0; i < letter->h; i++) {
        for (int j = 0; j < letter->w; j++) {
            if (!letter->pixels[i * letter->w + j]) continue;
            int index = (i + offY) * picW + (j + offX);
            pixels[index].red = 255;
            pixels[index].green = 255;
            pixels[index].blue = 255;
            pixels[index].alpha = 255;
        }
    }
}

+ (void)fixLetter:(Letter_T)letter withRow:(int)y andCol:(int)x
{
    // Fix the stuff if the first time touched
    if (!letter->touched) {
        letter->min.x = INT_MAX;
        letter->min.y = INT_MAX;
        letter->max.x = -1;
        letter->max.y = -1;
        letter->numCoords = 0;
        letter->touched = true;
    }

    // Check if I'm tiny or big
    if (x < letter->min.x) letter->min.x = x;
    if (y < letter->min.y) letter->min.y = y;
    if (x > letter->max.x) letter->max.x = x;
    if (y > letter->max.y) letter->max.y = y;
    letter->dims.x = letter->max.x - letter->min.x;
    letter->dims.y = letter->max.y - letter->min.y;
    letter->numCoords++;
}

+ (bool)loadLetters
{
    // Load the alphabet reference...
    UIImage *image = [UIImage imageNamed:@"alpha.png"];
    if (image == nil) {
        NSLog(@"Cannot load alphabet reference image");
        return false;
    }
    Pixel *pixels = [ReCameraStuff getPixels:image.CGImage];
    if (pixels == nil) {
        NSLog(@"Cannot get pixels for image");
        return false;
    }
    int h = CGImageGetHeight(image.CGImage), w = CGImageGetWidth(image.CGImage);

    // Get where things will be stored..
    bool *lums = malloc(sizeof(bool) * h * w);
    Letters_T ltrs = malloc(sizeof(struct Letters));
    if (lums == nil || ltrs == nil) {
        NSLog(@"Cannot malloc luminances array");
        return false;
    }
    bzero(ltrs, sizeof(struct Letters));
    ltrs->width = w;

    // Divide it into parts that will contain the different letters
    int letw = w / LET_COLS, leth = h / LET_ROWS;
    for (int r = 0; r < h; r++) {
        for (int c = 0; c < w; c++) {
            // Figure out the luminance stuff...
            Pixel pix = pixels[r * w + c];
            int lum = [ReCluster getPixelLuminance:&pix];
            lums[r * w + c] = lum < LUM_THRES;
            if (!lums[r * w + c]) continue;

            // Which letter is it
            int letter = r / letw * LET_COLS + c / leth;
            if (letter >= NUM_LETTERS) break;
            [ReLetterz fixLetter:&ltrs->letters[letter] withRow:r andCol:c];
        }
    }
    
    // Make boolean arrays for each letter...
    for (int i = 0; i < NUM_LETTERS; i++) {
        Letter ltr = ltrs->letters[i];
        bool *ltrLums = malloc(sizeof(bool) * ltr.dims.x * ltr.dims.y);
        for (int r = ltr.min.y; r < ltr.max.y; r++) {
            for (int c = ltr.min.x; c < ltr.max.x; c++) {
                int index = (r - ltr.min.y) * ltr.dims.x + (c - ltr.min.x);
                ltrLums[index] = lums[r * w + c];
            }
        }
        
        // Fill
        ltrs->ltrs[i].w = ltr.dims.x;
        ltrs->ltrs[i].h = ltr.dims.y;
        ltrs->ltrs[i].pixels = ltrLums;
    }
    
    // Fill the letters rotated...
    for (int i = NUM_LETTERS; i < NUM_LETTERS * 4; i++) {
        BetterLetter *ref =&ltrs->ltrs[i - NUM_LETTERS];
        bool *ltrLums = malloc(sizeof(bool) * ref->w * ref->h);
        for (int r = 0; r < ref->h; r++) {
            for (int c = 0; c < ref->w; c++) {
                int x = c - ref->w / 2,   y = ref->h / 2 - r;
                int xr = y,               yr = -x;
                int cr = xr + ref->h / 2, rr = ref->w / 2 - yr;
                ltrLums[rr * ref->h + cr] = ref->pixels[r * ref->w + c];
            }
        }
        
        // Fill
        ltrs->ltrs[i].w = ref->h;
        ltrs->ltrs[i].h = ref->w;
        ltrs->ltrs[i].pixels = ltrLums;
    }

    // Yay assign and leave
    ltrs->pixels = lums;
    letters = ltrs;
    return true;
}

// Fidns the probability that these are a match
+ (double)getProbability:(BetterLetter *)p1 with:(BetterLetter *)p2 test:(bool)t
{
    // Make sure that p2 has the larger width when at same height
    double ratio = p1->h  * 1.0 / p2->h;
    double sw2 = p2->w * ratio;
    if (sw2 < p1->w) return [ReLetterz getProbability:p2 with:p1 test:!t];
    
    // Get the offset for the smaller width (p1)
    double offset = (sw2 - p1->w) / 2.0;
    
    // Find the number of matches
    int count = 0;
    for (int r = 0; r < p1->h; r++) {
        for (int c = (int)offset; c < sw2 - (int)offset; c++) {
            int r1 = r, r2 = r / ratio;
            int c1 = c - offset, c2 = c / ratio;
            int i1 = r1 * p1->w + c1, i2 = r2 * p2->w + c2;
            count += p1->pixels[i1] == p2->pixels[i2] ? 1 : 0;
            
            // If there's a pixel in the test that's not in the alpha, penalize
            if ((t && p1->pixels[i1] && !p2->pixels[i2]) ||
                (!t && !p1->pixels[i1] && p2->pixels[i2])) {
                count -= 2;
            }
        }
    }
    return count * 1.0 / (p1->h * sw2);
}

+ (int)getLetterForCluster:(Cluster_T)cluster
{
    // Check loaded
    if (letters == nil) [ReLetterz loadLetters];
    int sums[NUM_LETTERS];
    bzero(sums, sizeof(int) * NUM_LETTERS);

    // Get the min and max
    Coords_T min = [ReCluster computeClusterMin:cluster];
    Coords_T max = [ReCluster computeClusterMax:cluster];
    int h = max->y - min->y + 1, w = max->x - min->x + 1;

    // Make a bitmap of the letter to identify
    bool *trial = malloc(sizeof(bool) * h * w);
    int count = cluster->coords.count;
    bzero(trial, sizeof(bool) * h * w);
    for (int i = 0; i < count; i++) {
        Coords_T coords = [ReCommonUtil dataToCoords:
            [cluster->coords objectAtIndex:i]];
        int ind = (coords->y - min->y) * w + (coords->x - min->x);
        trial[ind] = true;
    }
    
    // Form data structure
    BetterLetter test;
    test.pixels = trial;
    test.w = w;
    test.h = h;

    // Find the most probable letter
    double best = 0;
    int bestLtr = 0;
    for (int i = 0; i < NUM_LETTERS * 4; i++) {
        BetterLetter *ltr = &letters->ltrs[i];
        double prob = [ReLetterz getProbability:ltr with:&test test:false];
        if (i % NUM_LETTERS == 'O' - 'A')
            NSLog(@"O at %d is %f", i, prob);
        if (i % NUM_LETTERS == 'Q' - 'A')
            NSLog(@"Q at %d is %f", i, prob);
        if (prob > best) {
            best = prob;
            bestLtr = i;
        }
    }
    NSLog(@"Best is %c", (bestLtr % NUM_LETTERS) + 'A');
    return bestLtr;
}

+ (void) testClusters:(ClusterBag_T)clusters
{
    for (int i = 0; i < clusters->clusters.count; i++) {
        Cluster_T x = [ReCommonUtil dataToCluster:
            [clusters->clusters objectAtIndex:i]];
        [ReLetterz getLetterForCluster:x];
    }
}

+ (bool)isSymmetrical:(char)letter
{
    for (int i = 0; i < SYMM_LEN; i++) {
        if (SYMMETRICAL[i] == letter) return true;
    }
    return false;
}

+ (void)traverseTiles:(Tile_T)tile withClusters:(ClusterBag_T)clusters
    andRot:(int *)rot
{
    if (tile == nil || tile->visited) return;
    tile->visited = true;
    
    // Fill...
    NSData *data = [clusters->clusters objectAtIndex:tile->clusterId];
    Cluster_T cluster = [ReCommonUtil dataToCluster:data];
    int ltr = [ReLetterz getLetterForCluster:cluster];
    int letter = ltr % NUM_LETTERS;
    tile->rafi = 'A' + letter;
    
    // Vote for best rotation
    int ori = ltr / NUM_LETTERS;
    rot[ori]++;
    if ([ReLetterz isSymmetrical:tile->rafi]) rot[(ori + 2) % 4]++;
    
    // Continue on yo
    [ReLetterz traverseTiles:tile->up withClusters:clusters andRot:rot];
    [ReLetterz traverseTiles:tile->down withClusters:clusters andRot:rot];
    [ReLetterz traverseTiles:tile->left withClusters:clusters andRot:rot];
    [ReLetterz traverseTiles:tile->right withClusters:clusters andRot:rot];
}

// Changes the links on the tile to rotate by ori * 90 degrees
+ (void)rotateLinks:(Tile_T)tile toOri:(int)ori
{
    Tile_T links[] = { tile->up, tile->right, tile->down, tile->left };
    tile->up = links[(ori) % 4];
    tile->right = links[(1 + ori) % 4];
    tile->down = links[(2 + ori) % 4];
    tile->left = links[(3 + ori) % 4];
}

+ (void)rotateTiles:(Tile_T)tile toOri:(int)ori
{
    if (tile == nil || !tile->visited) return;
    tile->visited = false;
    [ReLetterz rotateLinks:tile toOri:ori];
    [ReLetterz rotateTiles:tile->left toOri:ori];
    [ReLetterz rotateTiles:tile->right toOri:ori];
    [ReLetterz rotateTiles:tile->up toOri:ori];
    [ReLetterz rotateTiles:tile->down toOri:ori];
}

+ (void)fillLettersForTile:(Tile_T)tile withClusters:(ClusterBag_T)clusters
{
    int rotationCount[4];
    bzero(rotationCount, sizeof(int) * 4);
    [ReLetterz traverseTiles:tile withClusters:clusters andRot:rotationCount];
    
    // Find the winning orientation...
    int maxRotate = 0, ori = 0;
    for (int i = 0; i < 4; i++) {
        if (rotationCount[i] > maxRotate) {
            maxRotate = rotationCount[i];
            ori = i;
        }
    }
    
    // Return if don't need to rotate connections, oterwise rotate
    if (ori == 0) return;
    [ReLetterz rotateTiles:tile toOri:ori];
}

@end
