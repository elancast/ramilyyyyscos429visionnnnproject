//
//  RootViewController.h
//  cos429
//
//  Created by Emily Lancaster on 1/2/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

#import "ReCameraStuff.h"

#import "LoadingView.h"

#import "ScoredView.h"

@interface RootViewController : UIViewController 


@property (nonatomic, retain) NSFetchedResultsController *
    fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (retain) ReCameraStuff *cameraStuff;

@property (retain) UIImageView *imageView;

@property (retain) LoadingView *loadingView;

@property (retain) ScoredView *scoredView;

@property bool waitingForClose;

@property (retain) UIImagePickerController *picker;

@end
