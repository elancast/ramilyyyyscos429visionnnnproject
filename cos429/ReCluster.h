//
//  ReCluster.h
//  cos429
//
//  Created by Emily Lancaster on 1/10/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef RECLUSTER_H
#define RECLUSTER_H

#include "ReCameraStuff.h"

@interface ReCluster : NSObject

typedef struct ClusterBag *ClusterBag_T;
typedef struct Cluster *Cluster_T;
typedef struct Coords *Coords_T;
typedef struct Image *Image_T;

// Cool
+ (ClusterBag_T)getClustersForImg:(Pixel *)pixels height:(int)h width:(int)w;

+ (Pixel *)getLummedPixels:(Pixel *)pixels 
                    height:(int)h width:(int)w 
                clusterBag:(ClusterBag_T)cls;

// Boring methods
+ (Coords_T)computeClusterCenter:(Cluster_T) clusty;
+ (Coords_T)computeClusterMax:(Cluster_T) clusty;
+ (Coords_T)computeClusterMin:(Cluster_T) clusty;

// Returns the pixel luminance, or 
+ (double)getPixelLuminance:(Pixel *)pixel;

typedef struct ClusterBag {
    NSMutableArray *clusters;
}   ClusterBag;

// Data structure to represent a point in the image
typedef struct Coords {
    int x;
    int y;
}   Coords;

// A cluster is a bag of coordinates in the image
typedef struct Cluster {
    NSMutableArray *coords;
}   Cluster;

typedef struct Image {
    Pixel *pixels;
    int height, width;
    ClusterBag_T bag;
    int *seen;
} Image;

@end

#endif
