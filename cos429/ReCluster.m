//
//  ReCluster.m
//  cos429
//
//  Created by Emily Lancaster on 1/10/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#include "ReCluster.h"
#include "ReCameraStuff.h"
#include "ReCommonUtil.h"

const double RMULT = .299;
const double GMULT = .587;
const double BMULT = .114;
const double MAX_CLUSTER_SIZE = 0.01;
const double MIN_CLUSTER_SIZE = 0.0001;
const double MERGE_FACTOR = 0.00005;

enum {
    LUM_THRES = 65,
    COLOR_THRES = 70,
    MAX_RECURSE = 1000,
    EMPTY = 0,
    INVALID = -1,
    MAX_EDGE_CLOSENESS = 10,
    ALPHA_THRES = 200,
    WHITE = 255
};

@implementation ReCluster

+ (void)freeCluster:(Cluster_T)cluster
{
    // TODO
}

+ (double)getSqdDistanceFrom:(Coords_T)c1 to:(Coords_T)c2
{
    double xdist = c1->x - c2->x;
    double ydist = c1->y - c2->y;
    return xdist * xdist + ydist * ydist;
}

+ (double)getPixelLuminance:(Pixel *)pixel
{
    if (pixel->alpha <= ALPHA_THRES) return WHITE;
    return RMULT * pixel->red + GMULT * pixel->green + BMULT * pixel->blue;
}

+ (int)getSeenValue:(Coords_T)coords inImg:(Image_T)img
{
    return img->seen[coords->y * img->width + coords->x];
}

// Returns whether the specified coordinate is empty
+ (bool)isEmpty:(Coords_T)coords inImg:(Image_T)img
{
    return img->seen[coords->y * img->width + coords->x] == EMPTY;
}

// Marks the specified coordinate as seen.
+ (void)markSeen:(Coords_T)coords inImg:(Image_T)img giveInd:(int)i
{
    img->seen[coords->y * img->width + coords->x] = i;
}

// Marks the specified coordinate as seen.
+ (bool)hasSameIndex:(Coords_T)coords inImg:(Image_T)img checkIndex:(int)i
{
    return img->seen[coords->y * img->width + coords->x] == i + 1;
}

+ (bool)isBlack:(Image_T)img at:(Coords_T)coords
{
    int index = coords->y * img->width + coords->x;
    struct Pixel *pixel = &img->pixels[index];
    return [ReCluster getPixelLuminance:pixel] < LUM_THRES ||
        (pixel->red < COLOR_THRES && pixel->blue < COLOR_THRES
        && pixel->green < COLOR_THRES);
}

+ (bool)isValidPixel:(Coords_T)coords inImg:(Image_T)img
{
    return coords->x >= 0 && coords->y >= 0 && coords->x < img->width
        && coords->y < img->height;
}

+ (void)followCluster:(Coords_T)coord outOf:(Image_T)img
    withCluster:(Cluster_T)cluster level:(int)lvl giveInd:(int)i
{
    // Hit max...?
    if (lvl >= MAX_RECURSE) return;

    // Check if should return...
    if (![ReCluster isValidPixel:coord inImg:img]) return;
    if (![ReCluster isEmpty:coord inImg:img]) return;

    // Mark it seen and check if valid luminance...
    [ReCluster markSeen:coord inImg:img giveInd:INVALID];
    if (![ReCluster isBlack:img at:coord]) return;

    // Add me to the cluster
    Coords_T coordsCpy = malloc(sizeof(struct Coords));
    if (coordsCpy == nil) {
        NSLog(@"Error allocating memory for coords");
        return;
    }
    coordsCpy->x = coord->x;
    coordsCpy->y = coord->y;
    [cluster->coords addObject:[ReCommonUtil coordsToData:coordsCpy]];
    [ReCluster markSeen:coord inImg:img giveInd:i + 1];

    // Check out my neighbors and return
    struct Coords nbr;
    nbr.x = coord->x - 1;
    nbr.y = coord->y;
    [ReCluster followCluster:&nbr outOf:img withCluster:cluster level:lvl + 1
        giveInd:i];
    nbr.x = coord->x + 1;
    [ReCluster followCluster:&nbr outOf:img withCluster:cluster level:lvl + 1
        giveInd:i];
    nbr.x = coord->x;
    nbr.y = coord->y + 1;
    [ReCluster followCluster:&nbr outOf:img withCluster:cluster level:lvl + 1
        giveInd:i];
    nbr.y = coord->y - 1;
    [ReCluster followCluster:&nbr outOf:img withCluster:cluster level:lvl + 1
        giveInd:i];
}

+ (Cluster_T)getCluster
{
    Cluster_T cluster = malloc(sizeof(struct Cluster));
    if (cluster == nil) {
        NSLog(@"Error mallocing cluster!");
        return nil;
    }
    cluster->coords = [[NSMutableArray alloc] init];
    return cluster;
}

+ (void)genNiceColor:(Pixel *)pix
{
    double h = (abs(rand()) % 361) / 60.0;
    double c = 1;
    double x = (h - (((int)h) / 2 * 2)) - 1;
    if (x < 0) x *= -1;
    x = c * (1 - x);
    
    // Find rgb on 0-1 scale
    double r = 0, g = 0, b = 0;
    if (h < 1) {
        r = c; g = x;
    } else if (h < 2) {
        r = x; g = c;
    } else if (h < 3) {
        g = c; b = x;
    } else if (h < 4) {
        g = x; b = c;
    } else if (h < 5) {
        r = x; b = c;
    } else {
        r = c; b = x;
    }
    
    // pixelize
    pix->red = (r * 255);
    pix->green = (g * 255);
    pix->blue = (b * 255);
}

+ (Pixel *)getLummedPixels:(Pixel *)pixels height:(int)h width:(int)w 
                clusterBag:(ClusterBag_T)cls
{
    // More convenient...
    struct Image img;
    img.pixels = pixels;
    img.height = h;
    img.width = w;

    // Clusters...
    if (cls == nil)
        cls = [ReCluster getClustersForImg:pixels height:h width:w];
        
    // Highlight
    for (int i = 0; i < cls->clusters.count; i++) {
        Cluster_T cl = [ReCommonUtil dataToCluster:[cls->clusters
            objectAtIndex:i]];
            
        // Generate a random color that's pretty...
        Pixel nice;
        [self genNiceColor:&nice];

        for (int j = 0; j < cl->coords.count; j++) {
            Coords_T c = [ReCommonUtil
                dataToCoords:[cl->coords objectAtIndex:j]];
            pixels[c->y * w + c->x].red = nice.red;
            pixels[c->y * w + c->x].green = nice.green;
            pixels[c->y * w + c->x].blue = nice.blue;
            pixels[c->y * w + c->x].alpha = 255;
        }
    }
    return pixels;
}

+ (void)mergeCluster:(int)into andIndex:(int)ind inImg:(Image_T)img
{
    // Mark the new cluster and add all pixels to this one...
    Cluster_T goodOne =
        [ReCommonUtil dataToCluster:[img->bag->clusters objectAtIndex:into]];
    Cluster_T other =
        [ReCommonUtil dataToCluster:[img->bag->clusters objectAtIndex:ind]];
    for (int i = 0; i < other->coords.count; i++) {
        NSData *coordData = [other->coords objectAtIndex:i];
        Coords_T coords = [ReCommonUtil dataToCoords:coordData];
        [ReCluster markSeen:coords inImg:img giveInd:into + 1];
        [goodOne->coords addObject:coordData];
    }

    // Remove from array. Can assume it's at a greater array than this so
    // shouldn't screw up the for loop below
    assert(ind > into);
    [other->coords removeAllObjects];

    // Free
    [ReCluster freeCluster:other];
}

+ (void)checkAroundPixel:(Coords_T)ref nowAt:(Coords_T)now inImg:(Image_T)img
{
    // If the pixel is invalid, EMPTY, or has the same cluster, return
    if (![ReCluster isValidPixel:now inImg:img]) return;
    int seen = [ReCluster getSeenValue:now inImg:img];
    int refSeen = [ReCluster getSeenValue:ref inImg:img];
    if (seen == EMPTY || (ref != now && seen == refSeen)) return;
    
    // If I'm really far, return...
    double distance = [ReCluster getSqdDistanceFrom:now to:ref];
    if (distance > MERGE_FACTOR * img->width * img->height) return;

    // If it has a cluster, merge themmm and return
    if (ref != now && seen > 0) {
        ///NSLog(@"Merging clusters %d and %d", seen, refSeen);
        [ReCluster mergeCluster:refSeen - 1 andIndex:seen - 1 inImg:img];
        return;
    }

    // Mark it as open and check around
    if (seen < 1) [ReCluster markSeen:now inImg:img giveInd:EMPTY];
    Coords c;
    c.x = now->x + 1; c.y = now->y;
    [ReCluster checkAroundPixel:ref nowAt:&c inImg:img];
    c.x = now->x - 1; c.y = now->y;
    [ReCluster checkAroundPixel:ref nowAt:&c inImg:img];
    c.x = now->x; c.y = now->y + 1;
    [ReCluster checkAroundPixel:ref nowAt:&c inImg:img];
    c.x = now->x; c.y = now->y - 1;
    [ReCluster checkAroundPixel:ref nowAt:&c inImg:img];
}

+ (bool)isCloseToBoundary:(Coords_T)coords inImg:(Image_T)img
{
    int left = MAX_EDGE_CLOSENESS, right = img->width - MAX_EDGE_CLOSENESS,
        bottom = img->height - MAX_EDGE_CLOSENESS;
    return coords->x <= left || coords->x >= right || coords->y <= left 
        || coords->y >= bottom;
}

// Returns true if the pixel is close to a boundary of the image
+ (bool)mergeWithNeighbors:(Cluster_T)cluster inImg:(Image_T)img
{
    bool close = false;
    for (int i = 0; i < cluster->coords.count; i++) {
        NSData *coordsData = [cluster->coords objectAtIndex:i];
        Coords_T coords = [ReCommonUtil dataToCoords:coordsData];
        [ReCluster checkAroundPixel:coords nowAt:coords inImg:img];
        close = close || [ReCluster isCloseToBoundary:coords inImg:img];
    }
    return close;
}

+ (void)mergeCloseClustersInImg:(Image_T)img
{
    // Maximimum clsuter size is function of image size
    int max = MAX_CLUSTER_SIZE * img->width * img->height;
    int min = MIN_CLUSTER_SIZE * img->width * img->height;
    
    // At the end of clustering, seen should have all INVALID pixels or pixels
    // in clusters in the seen array. This changes the INVALID to open in the
    // seen array to OPEN
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < img->bag->clusters.count; i++) {
        NSData *clusterData = [img->bag->clusters objectAtIndex:i];
        Cluster_T cluster = [ReCommonUtil dataToCluster:clusterData];
        bool close = [ReCluster mergeWithNeighbors:cluster inImg:img];
        
        // Check if good number of pixels and not cloes to bounds and keep if so
        int count = cluster->coords.count;
        if (close || count > max || count < min) continue;
        [arr addObject:[ReCommonUtil clusterToData:cluster]];
        ///NSLog(@"Kept %d", count);
        
        // TODO: Free stuff when not including it
    }
    
    // Take out the empty ones...
    img->bag->clusters = arr;
    // TODO: Free the old one
}

+ (ClusterBag_T)getClustersForImg:(Pixel *)pixels height:(int)h width:(int)w
{
    // Need a visited array, init with false...
    int *seen = malloc(sizeof(int) * w * h);
    if (seen == nil) {
        NSLog(@"Error mallocing for seen array");
        return nil;
    }
    bzero(seen, sizeof(int) * w * h);

    // Initialize a bag of clusters
    ClusterBag_T bag = malloc(sizeof(struct ClusterBag));
    if (bag == nil) {
        NSLog(@"Cannot allocate memory for cluster bag");
        return nil;
    }
    bag->clusters = [[NSMutableArray alloc] init];

    // More convenient...
    struct Image img;
    img.pixels = pixels;
    img.height = h;
    img.width = w;
    img.bag = bag;
    img.seen = seen;

    // A starting cluster
    Cluster_T cluster = [self getCluster];
    if (cluster == nil) return nil;

    // Run through and make some clusters
    NSLog(@"Running for size %d %d", w, h);
    for (int j = 0; j < h; j++) {
        ///if (j % 100 == 0) NSLog(@"J is %d", j);
        for (int i = 0; i < w; i++) {
            struct Coords coord;
            coord.x = i;
            coord.y = j;
            [ReCluster followCluster:&coord outOf:&img withCluster:cluster
                level:0 giveInd:bag->clusters.count];

            // If it's a cluster, add it and get another new cluster...
            if (cluster->coords.count == 0) continue;
            [bag->clusters addObject:[ReCommonUtil clusterToData:cluster]];
            cluster = [self getCluster];
            if (cluster == nil) return nil;
        }
    }
    NSLog(@"Got %d clusters", bag->clusters.count);

    // Merge close clusters...
    [ReCluster mergeCloseClustersInImg:&img];
    NSLog(@"Merged to have %d clusters", bag->clusters.count);
    return bag;
}

+ (Coords_T)computeClusterCenter:(Cluster_T) clusty {
    Coords_T min = [ReCluster computeClusterMin:clusty];
    Coords_T max = [ReCluster computeClusterMax:clusty];
    min->x = (min->x + max->x) / 2;
    min->y = (min->y + max->y) / 2;
    free(max);
    return min;
}

+ (Coords_T)computeClusterMax:(Cluster_T) clusty {
    int x = -1, y = -1;
    for (int i = 0; i < clusty->coords.count; i++) {
        NSData *data = [clusty->coords objectAtIndex:i];
        Coords_T coords = [ReCommonUtil dataToCoords:data];
        if (coords->x > x) x = coords->x;
        if (coords->y > y) y = coords->y;
    }
    Coords_T ret = malloc(sizeof(Coords));
    ret->x = x; ret->y = y;
    return ret;
}

// This mallocs! Be careful about memory.
+ (Coords_T)computeClusterMin:(Cluster_T) clusty {
    int x = INT32_MAX, y = INT32_MAX;
    for (int i = 0; i < clusty->coords.count; i++) {
        NSData *data = [clusty->coords objectAtIndex:i];
        Coords_T coords = [ReCommonUtil dataToCoords:data];
        if (coords->x < x) x = coords->x;
        if (coords->y < y) y = coords->y;
    }
    Coords_T ret = malloc(sizeof(Coords));
    ret->x = x; ret->y = y;
    return ret;
}

@end
