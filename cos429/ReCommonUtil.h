//
//  ReCommonUtil.h
//  cos429
//
//  Created by Rafael Romero on 1/11/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <Foundation/Foundation.h>


#ifndef RECOMMON_H
#define RECOMMON_H

#include "ReCluster.h"
#include "ReMst.h"

@interface ReCommonUtil : NSObject

+ (NSData*)clusterToData:(Cluster_T)cluster;
+ (NSData*)coordsToData:(Coords_T)coords;
+ (NSData*)tilesToData:(Tile_T)tiles;

+ (Cluster_T)dataToCluster:(NSData*)data;
+ (Coords_T)dataToCoords:(NSData*)data;
+ (Tile_T)dataToTiles:(NSData*)data;

+ (NSData*)dataFromStruct:(void *)param wLength:(int)len;
+ (void *)structFromData:(NSData *)data;

@end

#endif
