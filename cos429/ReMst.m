//
//  ReMst.m
//  cos429
//
//  Created by Emily Lancaster on 1/10/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#include "ReCommonUtil.h"
#include "ReMst.h"

@implementation ReMst

+ (Tile_T)getNewTile:(int)cluster withChar:(char)rafi
{
    // Get and fill
    Tile_T tile = malloc(sizeof(Tile));
    if (tile == nil) return nil;
    tile->clusterId = cluster;
    tile->rafi = rafi;
    
    // Haven't done these dirs yet...
    tile->horiz = false;
    tile->vert = false;
    
    // Connections...
    tile->up = nil;
    tile->down = nil;
    tile->left = nil;
    tile->right = nil;
    return tile;
}

+ (NSMutableArray *)computeCentersFromClusterBag:(ClusterBag_T)cb
{
    // dereference clusters from clusterBag
    NSMutableArray *clusters = cb->clusters;
    
    // calculate cluster centers
    NSMutableArray *centers = [[NSMutableArray alloc] 
                               initWithCapacity:[clusters count]];
    for (NSData* clusterData in clusters)
    {
        // was [centers addObject:(id)[ReCluster computeClusterCenter:cluster]];
        Cluster_T cluster = [ReCommonUtil dataToCluster:clusterData];
        Coords_T center = [ReCluster computeClusterCenter:cluster];
        NSData* centerData = [ReCommonUtil coordsToData:center];
        [centers addObject:centerData];
    }
    
    return centers;
    /// !! POTENTIAL MEMORY LEAK
}


+ (Pixel *)drawCenters:(NSMutableArray*)centers OntoImage:(CGImageRef)imgRef
{
    int w = CGImageGetWidth(imgRef);
    int h = CGImageGetHeight(imgRef);
    Pixel* orig = [ReCameraStuff getPixels:imgRef];
    
    Pixel* ret = malloc(w * h * sizeof(Pixel));
    bcopy(orig, ret, w * h * sizeof(Pixel));
    
    int DIM = 5;
    
    for (NSData* data in centers) // for each center
    {
        Coords_T coords = [ReCommonUtil structFromData:data];
        int x = coords->x, y = coords->y;
        for (int i = x-DIM; i <= x+DIM; i++) {
            for (int j = y-DIM; j < y+DIM; j++) {
                if (i < 0 || j < 0 || i > w || j > w)
                    continue;
                
                Pixel* pixel = [ReCameraStuff getPixel:ret width:w row:j col:i];
                pixel->red = 255; pixel->green = 0; pixel->blue = 0;
            }
        }
    }
    
    return ret;
}


+ (double)dist2_From:(Coords_T)c1 To:(Coords_T)c2
{
    int dx = c1->x - c2->x;
    int dy = c1->y - c2->y;
    
    return dx*dx + dy*dy;
}


typedef struct Pair *Pair_T;
struct Pair {
    int dist2;
    int ctr_i;
    int ctr_j;
};

+ (void)drawLineOnto:(Pixel *)pixels W:(int)w from:(Coords_T)a to:(Coords_T)b
{    
    int x0 = a->x, y0 = a->y;
    int x1 = b->x, y1 = b->y;
    
    int A = y0 - y1;
    int B = x1 - x0;
    int C = x0*y1 - x1*y0;
    
    for (int i = MIN(x0, x1) - (w/500); i <= MAX(x0, x1) + (w/500); i++)
        for (int j = MIN(y0, y1) - (w/500); j <= MAX(y0, y1) + (w/500); j++)
            if ( abs(A*i + B*j + C) <= w*w/8000 )
            {
                Pixel* p = [ReCameraStuff getPixel:pixels width:w row:j col:i];
                p->red = 255;
                p->green = 0;
                p->blue = 0;
            }
}

+ (void)selectionSort:(double *)arr count:(int)count
{
    for (int i = 0; i < count; i++)
    {
        int argmin = i;
        
        for (int j = i; j < count; j++)
        {
            if (arr[j] < arr[argmin])
                argmin = j;
        }
        
        double temp = arr[i];
        arr[i] = arr[argmin];
        arr[argmin] = temp;
    }
}

+ (int)direction:(double)angle givenCompass:(double *)rose
{
    double a0 = angle - 2*M_PI, a1 = angle, a2 = angle - 2*M_PI;
    
    double min = 10.0; int argmin = -1;
    
    for (int i = 0; i < 4; i++)
    {
        double closest = 10.0;
        closest = MIN(closest, fabs(a0 - rose[i]));
        closest = MIN(closest, fabs(a1 - rose[i]));
        closest = MIN(closest, fabs(a2 - rose[i]));
        
        if (closest < min)
        {
            min = closest;
            argmin = i;
        }
    }
    
    return argmin;
}

+ (void)TileCoordRecurse:(Tile_T)tile X:(int)x Y:(int)y
{
    if (tile == NULL || tile->visited)
        return;
    
    tile->x = x;
    tile->y = y;
    tile->visited = TRUE;
    
    [self TileCoordRecurse:tile->right X:x+1 Y:y  ];
    [self TileCoordRecurse:tile->up    X:x   Y:y+1];
    [self TileCoordRecurse:tile->left  X:x-1 Y:y  ];
    [self TileCoordRecurse:tile->down  X:x   Y:y-1];
}

+ (char)charFromClusterId:(int)clusterID
{
    if (clusterID < 26)
        return clusterID + 'A';
    else
        return clusterID-26 + 'a';
}

+ (double)angleFrom:(Coords_T)ca to:(Coords_T)cb
{
    double dx = cb->x - ca->x;
    double dy = cb->y - ca->y;
    
    dy *= -1;
    return atan2(dy, dx);
}

+ (Tile_T *)findMstForCenters:(NSMutableArray *)centers 
                   resultsOn:(Pixel *)pixels W:(int)w
{
    // Rafi's going to insert an n log n algorithm here
    // :D
    // ^^ lol
    
    /* ============================
       >> Create Pair-Wise Array <<
       ============================ */
    
    NSLog(@"Beginning Euclidean MST");
    
    int num = [centers count];
    
    NSMutableArray *pairs = [NSMutableArray alloc]; 
    pairs = [pairs initWithCapacity:(num*(num-1) / 2) ];
    
    for (int i = 0; i < num; i++) {
        for (int j = i+1; j < num; j++) 
        {
            NSData *centerD_i = [centers objectAtIndex:i];
            NSData *centerD_j = [centers objectAtIndex:j];
            Coords_T ctr_i = (Coords_T)[ReCommonUtil structFromData:centerD_i];
            Coords_T ctr_j = (Coords_T)[ReCommonUtil structFromData:centerD_j];
            
            int dist2 = [self dist2_From:ctr_i To:ctr_j];
            
            Pair_T pair = malloc(sizeof(struct Pair));
            pair->dist2 = dist2;
            pair->ctr_i = i;
            pair->ctr_j = j;
            
            NSData *data = [ReCommonUtil dataFromStruct:pair 
                                                wLength:sizeof(struct Pair)];
            [pairs addObject:data];
        }
    }
    
    /* ====================
       >> Sort the Array <<
       ==================== */
    
    NSArray *sortedArray;    
    sortedArray = [pairs sortedArrayUsingComparator:^(id a, id b) 
    {   
        // extract pair from data
        Pair_T pi = [ReCommonUtil structFromData:(NSData *)a];
        Pair_T pj = [ReCommonUtil structFromData:(NSData *)b];
                
        // make NSNumber from 
        NSNumber *ni = [NSNumber numberWithInt: pi->dist2 ];
        NSNumber *nj = [NSNumber numberWithInt: pj->dist2 ];
        /// TODO = have to free!
        
        return [ni compare:nj];
    }];
    
    /* =============================
       >> Run Kruskal's Algorithm <<
       ============================= */
    
    // GRAPH
    BOOL graph[num][num]; int count = 0;
    for (int i = 0; i < num; i++)
        for (int j = 0; j < num; j++)
            graph[i][j] = FALSE;
    // UNION-FIND
    int uf[num];
    for (int i = 0; i < num; i++)
        uf[i] = i;
    
    for (int i = 0; 
         count < num-1; 
         i++)
    {
        NSData *data = [sortedArray objectAtIndex:i];
        Pair_T pair = [ReCommonUtil structFromData:data];
        
        int a = pair->ctr_i;
        int b = pair->ctr_j;
        
        if (uf[a] == uf[b])
        {
            /// add reconsider code
            
            // already connected, so skip
            continue; 
        }
        
        // add edge to graph
        graph[a][b] = TRUE;
        graph[b][a] = TRUE;
        
        // connect UF components
        int ufa = uf[a], ufb = uf[b];
        for (int u = 0; u < num; u++)
            if (uf[u] == ufb)
                uf[u] = ufa;
        assert(uf[a] == uf[b]);
        
        // increment edge count
        count++;
    }
    
    /* ======================
       >> Draw MST Results <<
       ====================== */
        
    for (int i = 0; i < num; i++)
        for (int j = i+1; j < num; j++)
            if (graph[i][j])
            {                
                NSData *da = [centers objectAtIndex:i];
                NSData *db = [centers objectAtIndex:j];
                Coords_T ca = (Coords_T)[ReCommonUtil structFromData:da];
                Coords_T cb = (Coords_T)[ReCommonUtil structFromData:db];
                [self drawLineOnto:pixels W:w from:ca to:cb];
            }

    /* ==================================
       >> Determine Directional Angles <<
       ================================== */
    
    NSLog(@"Beginning Topology/Configuration Determination");
    
    double *angles = malloc((num-1) * sizeof(double));
    int k = 0;
    
    // gather up angles
    for (int i = 0; i < num; i++)
        for (int j = i+1; j < num; j++)
            if (graph[i][j])
            {
                NSData *da = [centers objectAtIndex:i];
                NSData *db = [centers objectAtIndex:j];
                Coords_T ca = (Coords_T)[ReCommonUtil structFromData:da];
                Coords_T cb = (Coords_T)[ReCommonUtil structFromData:db];
                
                ///angles[k++] = atan((ca->x - cb->x)*1.0 / (ca->y - cb->y));
                angles[k] = [self angleFrom:ca to:cb];
                if (angles[k] < -M_PI_2)
                    angles[k] += M_PI;
                if (angles[k] > M_PI_2)
                    angles[k] -= M_PI;
                
                k++;
            }
    
    // sort
    [self selectionSort:angles count:num-1];
    
    // project 3 groupings onto 2
    int num45jumps = 0;
    for (int i = 0; i < num - 2; i++)
    {
        if ((angles[i+1] - angles[i]) > M_PI/4.0)
            num45jumps++;
    }
    if (num45jumps == 2)
        for (int i = 0; i < num - 2; i++)
        {
            if ((angles[i+1] - angles[i]) > M_PI/4.0) {
                angles[i] += M_PI;
                break;
            }
            else
                angles[i] += M_PI;
        }
    [self selectionSort:angles count:num-1];
    
    // set up directions for angles
    int first_n;
    for (int i = 0; i < num - 2; i++)
        if ((angles[i+1] - angles[i]) > M_PI/4.0)
            first_n = i+1;
    
    double E, N, W, S;
    for (int i = 0; i < num - 1; i++) {
        if (i < first_n)
            E += angles[i];
        else
            N += angles[i];
    }
    E /= first_n;
    N /= (num-1 - first_n);
    
    W = E + M_PI;
    S = N + M_PI;
    
    if (E < -M_PI_4) {
        double temp = E; E = N; N = W; W = S; S = temp;
    }
    
    double compassRose[] = {E, N, W, S};
    
    /* =======================
       >> MAKE TILE LATTICE <<
       ======================= */
    
    // create tile per cluster
    Tile_T* tiles = calloc(num, sizeof(Tile_T));
    for (int i = 0; i < num; i++)
    {
        tiles[i] = malloc(sizeof(struct Tile));
        tiles[i]->clusterId = i;
        tiles[i]->visited = FALSE;
        
        tiles[i]->up = NULL;
        tiles[i]->down = NULL;
        tiles[i]->left = NULL;
        tiles[i]->right = NULL;
        
        tiles[i]->x = 200;
        tiles[i]->y = 200;

        tiles[i]->horiz = false;
        tiles[i]->vert = false;
    }
    
    enum { EAST, NORTH, WEST, SOUTH };
    
    // for each graph node
    for (int i = 0; i < num; i++)
        for (int j = 0; j < num; j++)
            if (graph[i][j]) // if edge
            {
                NSData *di = [centers objectAtIndex:i];
                NSData *dj = [centers objectAtIndex:j];
                Coords_T this = (Coords_T)[ReCommonUtil structFromData:di];
                Coords_T that = (Coords_T)[ReCommonUtil structFromData:dj];
                
                /*
                double dx = that->x - this->x;
                double dy = that->y - this->y;
                
                double angle = atan2(dy, dx);
                 */
                double angle = [self angleFrom:this to:that];
                if (angle < 0) angle += 2*M_PI;
                
                // assign tile according to direction
                int dir = [self direction:angle givenCompass:compassRose];
                switch (dir) {
                    case EAST:
                        tiles[i]->right = tiles[j];
                        break;
                    case NORTH:
                        tiles[i]->up = tiles[j];
                        break;
                    case WEST:
                        tiles[i]->left = tiles[j];
                        break;
                    case SOUTH:
                        tiles[i]->down = tiles[j];
                        break;
                        
                    default:
                        break;
                }
            }
              
    // set coordinates
    [self TileCoordRecurse:tiles[0] X:0 Y:0];
    
    // shift, so start at 0
    int minX = 0, minY = 0;
    for (int i = 0; i < num; i++) {
        minX = MIN(minX, tiles[i]->x);
        minY = MIN(minY, tiles[i]->y);
    }
    for (int i = 0; i < num; i++) {
        tiles[i]->x -= minX;
        tiles[i]->y -= minY;
    }
    
    // calculate maxes
    int maxX = -1, maxY = -1;
    for (int i = 0; i < num; i++) {
        maxX = MAX(maxX, tiles[i]->x);
        maxY = MAX(maxY, tiles[i]->y);
    }
    maxX++; maxY++;
    
    // make grid
    //Tile_T grid[maxX][maxY];
    Tile_T **grid = malloc(maxX * sizeof(Tile_T *));
    for (int x = 0; x < maxX; x++)
        grid[x] = malloc(maxY * sizeof(Tile_T));
    for (int x = 0; x < maxX; x++)
        for (int y = 0; y < maxY; y++)
            grid[x][y] = NULL; // init to NULL
    for (int i = 0; i < num; i++)
        grid[ tiles[i]->x ][ tiles[i]->y ] = tiles[i];
    
    // set tile pointers to grid neighbors
    for (int i = 0; i < num; i++) 
    {
        int x = tiles[i]->x; int y = tiles[i]->y;
        
        tiles[i]->right = x+1 == maxX ? NULL : grid[x+1][y  ];
        tiles[i]->up    = y+1 == maxY ? NULL : grid[x  ][y+1];
        tiles[i]->left  = x-1 < 0     ? NULL : grid[x-1][y  ];
        tiles[i]->down  = y-1 < 0     ? NULL : grid[x  ][y-1];
    }
    
    /// RESULTS
    
    
    char results[3*maxY][3*maxX+1];
    for (int x = 0; x < 3*maxX; x++)
        for (int y = 0; y < 3*maxY; y++)
            results[y][x] = ' ';
    for (int y = 0; y < 3*maxY; y++)
        results[y][3*maxX] = '\n';
    results[3*maxY-1][3*maxX] = '\0';
    
    for (int i = 0; i < num; i++) 
    {
        Tile_T tile = tiles[i];
        
        int x = 3*tile->x + 1, y = 3*maxY-1 - (3*tile->y + 1);
        results[ y ][ x ] = 'X';
        
        if (tile->right) results[ y   ][ x+1 ] = '>';
        if (tile->up)    results[ y-1 ][ x   ] = '^';
        if (tile->left)  results[ y   ][ x-1 ] = '<';
        if (tile->down)  results[ y+1 ][ x   ] = 'v';
    }
     
    
    NSLog(@"\n%s", (char*) results);
    
    for (int i = 0; i < num; i++) {
        if (tiles[i]->up != NULL && tiles[i]->up->down != tiles[i]) NSLog(@"Violation!");
    }
    
    if (tiles[0]->up != NULL) NSLog(@"Tile 0 points up");
    if (tiles[0]->down != NULL) NSLog(@"Tile 0 points down");
    
    
    for (int i = 0; i < num; i++) {
        tiles[i]->visited = FALSE; /// not visited
        tiles[i]->y = maxY-1 - tiles[i]->y; /// origin to top left
    }
    
    return tiles;
    /// must free tiles array    
}


+ (Cluster_T)createDummyCluster_X:(int)x Y:(int)y
{
    Cluster_T cluster = malloc(sizeof(struct Cluster));
    cluster->coords = [[NSMutableArray alloc] initWithCapacity:9];
    
    for (int i = x-1; i <= x+1; i++) {
        for (int j = y-1; j <= y+1; j++) 
        {
            Coords_T coords = malloc(sizeof(struct Coords));
            coords->x = i;
            coords->y = j;
            
            NSData* coordsData = [ReCommonUtil coordsToData:coords];
            [cluster->coords addObject:coordsData];
        }
    }
    
    return cluster;
}

+ (Coords_T)dummyCenter_X:(int)x Y:(int)y
{
    Coords_T coords = malloc(sizeof(struct Coords));
    coords->x = x;
    coords->y = y;
    
    return coords;
}

@end
