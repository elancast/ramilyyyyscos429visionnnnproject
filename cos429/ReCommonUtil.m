//
//  ReCommonUtil.m
//  cos429
//
//  Created by Rafael Romero on 1/11/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "ReCommonUtil.h"

@implementation ReCommonUtil

+ (NSData*)clusterToData:(Cluster_T)cluster
{
    return [NSData dataWithBytes:cluster length:sizeof(struct Cluster)];
}

+ (NSData*)coordsToData:(Coords_T)coords
{
    return [NSData dataWithBytes:coords length:sizeof(struct Coords)];
}

+ (NSData*)tilesToData:(Tile_T)tiles
{
    return [NSData dataWithBytes:tiles length:sizeof(struct Tile)];
}

// v2.0
+ (NSData*)dataFromStruct:(void *)param wLength:(int)len
{
    return [NSData dataWithBytes:param length:len];
}




+ (Cluster_T)dataToCluster:(NSData*)data
{
    return (Cluster_T)data.bytes;
}

+ (Coords_T)dataToCoords:(NSData*)data
{
    return (Coords_T)data.bytes;
}

+ (Tile_T)dataToTiles:(NSData*)data
{
    return (Tile_T)data.bytes;
}

// v2.0
+ (void *)structFromData:(NSData *)data
{
    return (void *)data.bytes;
}

@end
