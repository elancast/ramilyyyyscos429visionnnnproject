//
//  ScoredView.m
//  cos429
//
//  Created by Emily Lancaster on 1/15/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import "ScoredView.h"

@implementation ScoredView

@synthesize ScoreTable;
@synthesize navbar;
@synthesize imageView;

@synthesize wordlist = _wordlist;
@synthesize img = _img;

enum { IMGHEIGHT = 205, NAVBARHEIGHT = 44 };

- (void)addImage:(UIImage *)img
{
    // Scale it to the height...
    int newH = IMGHEIGHT;
    int h = img.size.height, w = img.size.width;
    int newW = w * newH / h;
    
    // Check if w is > than screen w...
    int scrW = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    if (newW > scrW) {
        newW = scrW;
        newH = h * newW / w;
    }
    
    // Find height center
    int hcent = NAVBARHEIGHT + IMGHEIGHT / 2;
    
    // Add to image view
    self.imageView.bounds = CGRectMake(0, 0, newW, newH);
    self.imageView.image = img;
    self.imageView.center = CGPointMake(scrW / 2, hcent);
}

- (void)initGameEnd:(WordList *)words withImg:(UIImage *)img
{
    if (words == nil) return;

    // remember
    _img = img;
    _wordlist = words;
    [ScoreTable reloadData];
    int score = words->totalScore;

    // Set the title
    self.navbar.title = [NSString stringWithFormat:@"Your score was %d", score];
    
    // Show the image
    [self addImage:img];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _wordlist = nil;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Set top bar to have score
    //self.navbar.title = @"Your score was 20";
    
    // And a back button
    UIBarButtonItem *back = [[UIBarButtonItem alloc] 
        initWithTitle:@"Back" style:UIBarButtonItemStyleDone
        target:self action:@selector(doBack)];
    [back autorelease];
    self.navbar.leftBarButtonItem = back;
    
    // Show stuff
    [self initGameEnd:_wordlist withImg:_img];
}

- (void)doBack
{
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:true];
}

- (void)viewDidUnload
{
    [self setScoreTable:nil];
    [self setNavbar:nil];
    [self setImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//
// Table datasource methods
//

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    NSString *str = [NSString stringWithFormat:@"%3d %s",
        _wordlist->scores[row], _wordlist->words[row]];
    UITableViewCell *cell = [[UITableViewCell alloc] 
        initWithStyle:UITableViewCellStyleDefault
        reuseIdentifier:@"acell"];
    cell.textLabel.text = str;
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    [cell autorelease];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _wordlist == nil ? 0 : _wordlist->len;
}

//
// Table delegate methods
//
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

// Deconstructor
- (void)dealloc {
    [ScoreTable release];
    [navbar release];
    [imageView release];
    [super dealloc];
}



@end
