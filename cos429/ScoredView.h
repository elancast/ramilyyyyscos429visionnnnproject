//
//  ScoredView.h
//  cos429
//
//  Created by Emily Lancaster on 1/15/12.
//  Copyright 2012 Princeton University. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef SCOREDVIEW_H
#define SCOREDVIEW_H

@interface ScoredView : UIViewController 
    <UITableViewDataSource, UITableViewDelegate> {
    UITableView *ScoreTable;
        UINavigationItem *navbar;
        UIImageView *imageView;
}

typedef struct WordList WordList;
struct WordList {
    char **words;
    int *scores;
    int len, cap;
    int totalScore;
};

@property (nonatomic, retain) IBOutlet UITableView *ScoreTable;

@property (nonatomic, retain) IBOutlet UINavigationItem *navbar;

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@property (retain) UIImage *img;
@property WordList *wordlist;


- (void)initGameEnd:(WordList *)words withImg:(UIImage *)img;

@end

#endif